import React, { Component } from 'react'
import TransparentButton from '../components/Buttons/TransparentButton'

export default class UserActivation extends Component {
  constructor(props) {
    super(props)
    this.state = {
      message: props.message,
    }
  }

  render() {
    const { message } = this.state
    return (
      <div
        style={{
          width: '100%',
          height: '100%',
          textAlign: 'center',
          color: '#fff',
        }}
      >
        <h2> {message}</h2>
        <TransparentButton
          btnstyle="background-button"
          baseUrl="/landing"
          title="Home Page"
        />
      </div>
    )
  }
}
