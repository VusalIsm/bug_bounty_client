import React, { Component, Fragment } from 'react'
import { Switch, Route } from 'react-router-dom'
import PrivateNavbar from '../components/Navbars/PrivateNavbar'
import Leaderboard from '../components/Layouts/Dashboard/Leaderboard'
import BountiesList from '../components/Layouts/Dashboard/BountiesList'
import HackerProfile from '../components/Layouts/Dashboard/HackerProfile'
import CompanyProfile from '../components/Layouts/Dashboard/CompanyProfile'
import routes from '../routes'
import MyProfileHacker from '../components/Layouts/Dashboard/MyProfileHacker'
import MyProfileCompany from '../components/Layouts/Dashboard/MyProfileCompany'
import JoinBugBounty from '../components/Layouts/Dashboard/JoinBugBounty'
import BountyPage from '../components/Layouts/Dashboard/BountyPage'
import { SnackbarProvider } from 'material-ui-snackbar-provider'
import Submissions from '../components/Layouts/Dashboard/Submissions'
import CompanySubmissions from '../components/Layouts/Dashboard/CompanySubmissions'

class Dashboard extends Component {
  state = {
    dashboardList: [],
  }

  componentDidMount() {
    const dashboardList =
      localStorage.getItem('status') === 'HACKER'
        ? routes.HACKER_DASHBOARD
        : routes.COMPANY_DASHBOARD

    this.setState({
      dashboardList: dashboardList,
    })
  }

  render() {
    return (
      <Fragment>
        <SnackbarProvider
          SnackbarProps={{
            autoHideDuration: 2500,
          }}
        >
          <PrivateNavbar dashboardList={this.state.dashboardList} />
          <Switch>
            <Route path="/dashboard/leaderboard" component={Leaderboard} />
            <Route path="/dashboard/bounties" component={BountiesList} />
            <Route path="/dashboard/hacker/:id" component={HackerProfile} />
            <Route path="/dashboard/company/:id" component={CompanyProfile} />
            <Route
              path="/dashboard/hacker-profile"
              component={MyProfileHacker}
            />
            <Route path="/dashboard/bounty/:bountyId" component={BountyPage} />
            <Route
              path="/dashboard/company-profile"
              component={MyProfileCompany}
            />
            <Route path="/dashboard/submissions" component={Submissions} />
            <Route
              path="/dashboard/bounty-submissions"
              component={CompanySubmissions}
            />
            <Route
              path="/dashboard/join-bug-bounty"
              component={JoinBugBounty}
            />
          </Switch>
        </SnackbarProvider>
      </Fragment>
    )
  }
}

export default Dashboard
