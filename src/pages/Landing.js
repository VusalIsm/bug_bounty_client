import React from 'react'
import PublicNavbar from '../components/Navbars/PublicNavbar'
import LandingContent from '../components/Layouts/Landing/LandingContent'
import { Route, Switch } from 'react-router-dom'
import LoginContent from '../components/Layouts/Landing/LoginContent'
import RegisterContent from '../components/Layouts/Landing/RegisterContent'
import { SnackbarProvider } from 'material-ui-snackbar-provider'

class Landing extends React.Component {
  render() {
    return (
      <React.Fragment>
        <SnackbarProvider
          SnackbarProps={{
            autoHideDuration: 2500,
          }}
        >
          <PublicNavbar />

          <Switch>
            <Route exact path="/landing">
              <LandingContent />
            </Route>
            <Route exact path="/landing/login">
              <LoginContent />
            </Route>
            <Route exact path="/landing/register">
              <RegisterContent />
            </Route>
          </Switch>
        </SnackbarProvider>
      </React.Fragment>
    )
  }
}

export default Landing
