import React from 'react'
import { Route, Switch } from 'react-router-dom'

import Container from '@material-ui/core/Container'

import PublicNavbar from '../components/Navbars/PublicNavbar'
import PolicyContent from '../components/Layouts/Policy/PolicyContent'
import Footer from '../components/Footer/Footer'

class Policy extends React.Component {
  render() {
    return (
      <React.Fragment>
        <PublicNavbar />

        <Switch>
          <Route exact path="/policy">
            <Container
              maxWidth="lg"
              style={{
                height: '80vh',
                color: '#fff',
              }}
            >
              <PolicyContent />

              <Footer />
            </Container>
          </Route>
        </Switch>
      </React.Fragment>
    )
  }
}

export default Policy
