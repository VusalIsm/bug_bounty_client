import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import Check from '@material-ui/icons/Check'

import VpnKeyIcon from '@material-ui/icons/VpnKey'
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser'
import LanguageIcon from '@material-ui/icons/Language'

import StepConnector from '@material-ui/core/StepConnector'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { downloadMessageAsPlainText } from '../shared/helpers/File'
import { savePublicKey } from '../shared/helpers/Request'
import { generateAndExportKeys } from '../shared/helpers/RSA'
import LoadingButton from '../components/Buttons/LoadingButton'
import { SnackbarProvider, useSnackbar } from 'material-ui-snackbar-provider'
import { Container } from '@material-ui/core'

import PrivateKey from './src/private-key.png'
import AddDomain from './src/add-domain.png'
import BountiesList from './src/bounties-list.png'
import BountyPage from './src/bounty-page.png'
import CompanyDashboard from './src/company-dashboard.png'
import CompanyMyProfile from './src/company-my-profile.png'
import Domains from './src/domains.png'
import JoinBugBounty from './src/join-bug-bounty.png'
import TransparentButton from '../components/Buttons/TransparentButton'

const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    },
  },
  completed: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: '#eaeaf0',
    borderRadius: 1,
  },
})(StepConnector)

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: '#ccc',
    zIndex: 1,
    color: '#fff',
    width: 50,
    height: 50,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    backgroundImage:
      'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
  },
  completed: {
    backgroundImage:
      'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
  },
})

function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles()
  const { active, completed } = props

  const icons = {
    1: <VpnKeyIcon />,
    2: <VerifiedUserIcon />,
    3: <LanguageIcon />,
  }

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {icons[String(props.icon)]}
    </div>
  )
}

ColorlibStepIcon.propTypes = {
  /**
   * Whether this step is active.
   */
  active: PropTypes.bool,
  /**
   * Mark the step as completed. Is passed to child components.
   */
  completed: PropTypes.bool,
  /**
   * The label displayed in the step icon.
   */
  icon: PropTypes.node,
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  mainContainer: {
    backgroundColor: '#fff',
    borderRadius: '10px',
    padding: '25px',
  },
  finishedTypo: {
    background:
      'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    WebkitBackgroundClip: 'text',
    WebkitTextFillColor: 'transparent',
    textTransform: 'uppercase',
    fontSize: '50px',
    padding: '40px 0px',
  },
  justifyCenter: {
    display: 'flex',
    justifyContent: 'center',
  },
  textAlignCenter: {
    textAlign: 'center',
  },
}))

function getSteps() {
  return ['Generate Key Pair', 'Verify Some Domains', 'Join Your First Bounty']
}

const stepStyles = makeStyles((theme) => ({
  generalStepStyle: {
    background:
      'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    WebkitBackgroundClip: 'text',
    WebkitTextFillColor: 'transparent',
  },
  upperCaseCenter: {
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  textAlignCenter: {
    textAlign: 'center',
  },
  generalFigureStyle: {
    textAlign: 'center',
    padding: '15px 0px',
  },
  contentContainer: {
    justifyContent: 'center',
    maxWidth: '1000px',
  },
  seventeen: {
    fontSize: '17px',
  },
  pdTop: {
    paddingTop: '25px',
  },
  twenty: {
    fontSize: '20px',
  },
  maxWidthHeightHundred: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
  specialLink: {
    color: '#c3073f',
    textDecoration: 'none',
  },
}))

function Generate(props) {
  const classes = stepStyles()
  const snackbar = useSnackbar()
  const generateAndSaveKeyPairs = () => {
    return generateAndExportKeys().then((pair) => {
      const publicKeyString = pair.publicKey
      const privateKeyString = pair.privateKey

      return savePublicKey({
        public_key: publicKeyString,
      })
        .then((response) => {
          downloadMessageAsPlainText(privateKeyString, 'PRIVATE_KEY.txt')
          snackbar.showMessage(
            'Your keys are successfully generated. Please save your key securely, you will need this key when you open the reports'
          )
        })
        .catch((err) => {
          if (Array.isArray(err.error)) snackbar.showMessage(err.error[0])
          else snackbar.showMessage(err.error)
        })
    })
  }
  return (
    <React.Fragment>
      <Container className={classes.pdTop}>
        <Typography variant="h5" className={classes.generalStepStyle}>
          <ol start="1">
            <li>
              To complete the setup process for this company account, you have
              to first generate and store your public - private key pair in your
              local computer.
            </li>
          </ol>

          <br />
          <br />
        </Typography>
      </Container>

      <Container className={classes.contentContainer}>
        <Typography variant="h6" className={classes.upperCaseCenter}>
          <strong>
            Please click the button below to generate and download a unique key
            pair for your company.
          </strong>
        </Typography>
        <LoadingButton
          title="Generate your keys"
          sendRequest={generateAndSaveKeyPairs}
        />
        <div className={classes.textAlignCenter}>
          <Typography variant="subtitle2" className={classes.seventeen}>
            If you have gotten your key pair like so,
          </Typography>
        </div>
        <div className={classes.generalFigureStyle}>
          <img alt="" src={PrivateKey} />
        </div>
        <div className={classes.textAlignCenter}>
          <Typography variant="subtitle2" className={classes.seventeen}>
            Then you are good to go onto the next step.
          </Typography>
        </div>
      </Container>
    </React.Fragment>
  )
}

function Verify(props) {
  const classes = stepStyles()
  const snackbar = useSnackbar()

  return (
    <React.Fragment>
      <Container className={classes.pdTop}>
        <Typography variant="h5" className={classes.generalStepStyle}>
          <ol start="2">
            <li>
              Moving on, you need to verify some domains to later join bug
              bounty program(s). You can see the tutorial below on how to verify
              a domain.
            </li>
          </ol>
          <br />
          <br />
        </Typography>
      </Container>

      <Container className={classes.contentContainer}>
        <div>
          <Typography variant="subtitle2" className={classes.twenty}>
            <ul>
              <li>
                After finishing this starter guide, you will be redirected to
                Hacker Leaderboard page of where you will see the custom
                dashboard for company pages.Then, please enter "My Profile"
                section.
              </li>
            </ul>
          </Typography>
        </div>

        <div className={classes.generalFigureStyle}>
          <img
            alt=""
            className={classes.maxWidthHeightHundred}
            src={CompanyDashboard}
          />
        </div>

        <div>
          <Typography variant="subtitle2" className={classes.twenty}>
            <ul>
              <li>
                Below you will see an empty list.You can probably guess that
                this is your domains' list.You can here see both verified and
                unverified domains of your company next to their verification
                keys.
              </li>
            </ul>
          </Typography>
        </div>

        <div className={classes.generalFigureStyle}>
          <img
            alt=""
            className={classes.maxWidthHeightHundred}
            src={CompanyMyProfile}
          />
        </div>

        <div>
          <Typography variant="subtitle2" className={classes.twenty}>
            <ul>
              <li>Here is an example of some company's domain list :</li>
            </ul>
          </Typography>
        </div>
        <div className={classes.generalFigureStyle}>
          <img className={classes.maxWidthHeightHundred} alt="" src={Domains} />
        </div>

        <div>
          <Typography variant="subtitle2" className={classes.twenty}>
            <ul>
              <li>
                Clicking the plus icon in the top right corner, you can add
                domains for your company, and verify them for later use on
                bounty programs.
              </li>
            </ul>
          </Typography>
        </div>

        <div className={classes.generalFigureStyle}>
          <img
            className={classes.maxWidthHeightHundred}
            alt=""
            src={AddDomain}
          />
        </div>
      </Container>
    </React.Fragment>
  )
}

function Join(props) {
  const classes = stepStyles()
  const snackbar = useSnackbar()

  return (
    <React.Fragment>
      <Container style={{ paddingTop: '25px' }}>
        <Typography variant="h5" className={classes.generalStepStyle}>
          <ol start="3">
            <li>
              Lastly, if you want to be a relatively secure company, you will
              have to join at least one bounty program.You can look through
              below steps to do it.
            </li>
          </ol>
          <br />
          <br />
        </Typography>
      </Container>

      <Container className={classes.contentContainer}>
        <div>
          <Typography variant="subtitle2" className={classes.twenty}>
            <ul>
              <li>
                Again, when you are on the dashboard page after finishing this
                setup, please enter "Join Bug Bounty" section
              </li>
            </ul>
          </Typography>
        </div>

        <div className={classes.generalFigureStyle}>
          <img
            alt=""
            className={classes.maxWidthHeightHundred}
            src={CompanyDashboard}
          />
        </div>

        <div>
          <Typography variant="subtitle2" className={classes.twenty}>
            <ul>
              <li>
                You will have to fill the form to join your first
                program.Program name, domains ( or at least one domain ), policy
                of your program and corresponding bounty values for low, medium
                and high prices should be filled in.You can also mark the
                Private box as checked, if you want your bounty to be
                private.You can learn more about this matter on{' '}
                <a className={classes.specialLink} href="/policy">
                  Website Policy and Guidelines
                </a>
                .<br />
                <br />
                Please note that <i>only</i> verified domains will be seen as
                option in the "Domains" list box.
              </li>
            </ul>
          </Typography>
        </div>

        <div className={classes.generalFigureStyle}>
          <img
            alt=""
            className={classes.maxWidthHeightHundred}
            src={JoinBugBounty}
          />
        </div>

        <div>
          <Typography variant="subtitle2" className={classes.twenty}>
            <ul>
              <li>
                Finally after joining your first Bug Bounty Program, you can
                check it out at the Bounties List section through the navigation
                bar.
              </li>
            </ul>
          </Typography>
        </div>
        <div className={classes.generalFigureStyle}>
          <img
            alt=""
            className={classes.maxWidthHeightHundred}
            src={BountiesList}
          />
        </div>

        <div>
          <Typography variant="subtitle2" className={classes.twenty}>
            <ul>
              <li>
                And this is your bounty's general page.This is how other hackers
                ( or testers ) will see and submit report for your bounty
                program.
              </li>
            </ul>
          </Typography>
        </div>

        <div className={classes.generalFigureStyle}>
          <img
            alt=""
            className={classes.maxWidthHeightHundred}
            src={BountyPage}
          />
        </div>
      </Container>
    </React.Fragment>
  )
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return <Generate />
    case 1:
      return <Verify />
    case 2:
      return <Join />
    default:
      return 'Unknown step'
  }
}

export default function ApplicationGuide() {
  const classes = useStyles()
  const [activeStep, setActiveStep] = React.useState(0)
  const steps = getSteps()

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  const handleReset = () => {
    setActiveStep(0)
  }

  return (
    <Container maxWidth="lg" className={classes.mainContainer}>
      <SnackbarProvider
        SnackbarProps={{
          autoHideDuration: 2500,
        }}
      >
        <div className={classes.root}>
          <Stepper
            alternativeLabel
            activeStep={activeStep}
            connector={<ColorlibConnector />}
          >
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel StepIconComponent={ColorlibStepIcon}>
                  {label}
                </StepLabel>
              </Step>
            ))}
          </Stepper>
          <div>
            {activeStep === steps.length ? (
              <React.Fragment>
                <div className={classes.textAlignCenter}>
                  <Typography className={classes.finishedTypo}>
                    All steps completed - you&apos;re finished
                  </Typography>
                </div>
                <div className={classes.justifyCenter}>
                  <TransparentButton
                    btnstyle="go-to-your-profile"
                    baseUrl="/dashboard"
                    title="Go To Your Profile"
                  />
                </div>
                <Button onClick={handleReset} className={classes.button}>
                  Go Back to First Step
                </Button>
              </React.Fragment>
            ) : (
              <div>
                <Typography className={classes.instructions}>
                  {getStepContent(activeStep)}
                </Typography>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Back
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                  </Button>
                </div>
              </div>
            )}
          </div>
        </div>
      </SnackbarProvider>
    </Container>
  )
}
