import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import './index.css'
import Landing from './pages/Landing'
import Dashboard from './pages/Dashboard'
import UserActivation from './pages/UserActivation'
import Policy from './pages/Policy'
import ApplicationGuide from './pages/ApplicationGuide'

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Redirect exact from="/" to="/landing" />
      <Redirect exact from="/dashboard" to="/dashboard/leaderboard" />
      <Route path="/landing" component={Landing} />
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/application-guide" component={ApplicationGuide} />
      <Route
        exact
        path="/activated"
        component={() => <UserActivation message="Your account is activated" />}
      />
      <Route
        exact
        path="/postregister"
        component={() => (
          <UserActivation message="Please check your email and activate your account" />
        )}
      />

      <Route exact path="/policy" component={Policy} />
    </Switch>
  </BrowserRouter>,
  document.getElementById('root')
)
