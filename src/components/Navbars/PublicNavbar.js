import React, { Component } from 'react'
import NavButton from '../Buttons/NavButton'
import TransparentButton from '../Buttons/TransparentButton'
import './Navbars.css'
import { withRouter } from 'react-router-dom'

class PublicNavbar extends Component {
  constructor() {
    super()

    this.state = {
      navBtnStyle: 'nav-button',
      bckBtnStyle: 'background-button',
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', () => {
      const isTop = window.scrollY > 100
      if (document.getElementById('nav')) {
        if (isTop) {
          document.getElementById('nav').classList.add('scrolled')
          document
            .getElementById('logo-normal')
            .classList.add('scrolled-logo-normal')
          document.getElementById('logo-black').classList.remove('logo-black')
          this.setState({
            bckBtnStyle: 'background-button-alter',
            navBtnStyle: 'nav-button-alter',
          })
        } else {
          document.getElementById('nav').classList.remove('scrolled')
          document
            .getElementById('logo-normal')
            .classList.remove('scrolled-logo-normal')
          document.getElementById('logo-black').classList.add('logo-black')
          document.getElementsByClassName('NavButton').btnstyle = 'nav-button'
          this.setState({
            bckBtnStyle: 'background-button',
            navBtnStyle: 'nav-button',
          })
        }
      }
    })
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', null)
  }

  render() {
    return (
      <nav id="nav" className="nav-style">
        <ul>
          <li>
            <a id="logo-normal" href="/landing">
              <img
                className="pd"
                src={process.env.PUBLIC_URL + '/logotypes/logo.svg'}
                alt="SUZ"
              />
            </a>
            <a id="logo-black" className="logo-black" href="/landing">
              <img
                className="pd"
                src={process.env.PUBLIC_URL + '/logotypes/lightgrey.svg'}
                alt="SUZ"
              />
            </a>
          </li>
        </ul>
        <ul>
          <li>
            <NavButton
              btnstyle={this.state.navBtnStyle}
              baseUrl=""
              title="Home"
            />
          </li>
          <li>
            <NavButton
              btnstyle={this.state.navBtnStyle}
              baseUrl=""
              anchor="how-it-works"
              title="How it works"
            />
          </li>
          <li>
            <NavButton
              btnstyle={this.state.navBtnStyle}
              baseUrl=""
              anchor="here-is-our-team"
              title="Our team"
            />
          </li>
          <li>
            <NavButton
              btnstyle={this.state.navBtnStyle}
              baseUrl=""
              anchor="contact-us"
              title="Contact"
            />
          </li>
        </ul>
        <ul>
          <li>
            <TransparentButton
              btnstyle={this.state.bckBtnStyle}
              baseUrl="/landing/login"
              title="Login"
            />
          </li>
          <li>
            <TransparentButton
              btnstyle={this.state.bckBtnStyle}
              baseUrl="/landing/register"
              title="Register"
            />
          </li>
        </ul>
      </nav>
    )
  }
}

export default withRouter(PublicNavbar)
