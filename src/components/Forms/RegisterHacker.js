import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Avatar from '@material-ui/core/Avatar'
import TextField from '@material-ui/core/TextField'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import LoadingButton from '../Buttons/LoadingButton'
import { withSnackbar } from 'material-ui-snackbar-provider'

const styles = (theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '70%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
})

class RegisterHacker extends Component {
  constructor(props) {
    super(props)
    const { postData, status } = props
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      username: '',
      home_address: '',
      voen: '',
      password: '',
      passwordConfirmation: '',
      postData: postData,
      status: status,
      termsAccepted: false,
    }
  }

  setTermsAccepted = () => {
    this.setState({ termsAccepted: !this.state.termsAccepted })
  }

  handleForm = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    })
  }

  submitForm = async () => {
    const { postData, status } = this.state
    return postData(this.state)
      .then(() => {
        this.props.history.push('/postregister')
      })
      .catch((err) => {
        var errorMessage = null
        if (Array.isArray(err.error)) errorMessage = err.error[0]
        else errorMessage = err.error

        this.props.snackbar.showMessage(
          errorMessage ? errorMessage : 'Please try again'
        )
      })
  }

  render() {
    const { classes } = this.props
    const {
      first_name,
      last_name,
      email,
      username,
      home_address,
      voen,
      password,
      passwordConfirmation,
    } = this.state

    return (
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item md={12} lg={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="first_name"
                label="First Name"
                name="first_name"
                value={first_name}
                onChange={this.handleForm}
                autoComplete="first_name"
                autoFocus
              />
            </Grid>
            <Grid item md={12} lg={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="last_name"
                label="Last Name"
                name="last_name"
                value={last_name}
                onChange={this.handleForm}
                autoComplete="last_name"
              />
            </Grid>
            <Grid item md={12} lg={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="E-mail Address"
                name="email"
                value={email}
                onChange={this.handleForm}
                autoComplete="email"
              />
            </Grid>
            <Grid item md={12} lg={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                value={username}
                onChange={this.handleForm}
                autoComplete="username"
              />
            </Grid>
            <Grid item md={12} lg={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="home_address"
                label="Home Address"
                name="home_address"
                value={home_address}
                onChange={this.handleForm}
                autoComplete="home_address"
              />
            </Grid>
            <Grid item md={12} lg={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="voen"
                label="Your Voen Number"
                name="voen"
                value={voen}
                onChange={this.handleForm}
                autoComplete="voen"
              />
            </Grid>
            <Grid item md={12} lg={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                value={password}
                onChange={this.handleForm}
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
              />
            </Grid>
            <Grid item md={12} lg={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="passwordConfirmation"
                value={passwordConfirmation}
                onChange={this.handleForm}
                label="Re-enter your Password"
                type="password"
                id="passwordConfirmation"
                autoComplete="current-password"
              />
            </Grid>

            <Grid container justify="center">
              <Grid item>
                <Link target="_blank" href="/policy" variant="body2">
                  {'Read the full Policy of Team SecUrZone Here'}
                </Link>
              </Grid>
            </Grid>
            <Grid container justify="center">
              <Grid item>
                <FormControlLabel
                  control={<Checkbox justify="center" color="primary" />}
                  label="I agree to the Terms & Conditions"
                  onChange={this.setTermsAccepted}
                />
              </Grid>
            </Grid>
            <div style={{ width: '100%' }}>
              <LoadingButton
                title="Register Your Account"
                sendRequest={this.submitForm}
              />
            </div>

            <Grid container justify="flex-end">
              <Grid item>
                <Link href="/landing/login" variant="body2">
                  {'Already have an account? Sign In'}
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </form>
      </div>
    )
  }
}

export default withSnackbar()(withStyles(styles)(withRouter(RegisterHacker)))
