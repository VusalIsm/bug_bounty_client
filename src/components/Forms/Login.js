import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Avatar from '@material-ui/core/Avatar'
import TextField from '@material-ui/core/TextField'
import LoadingButton from '../Buttons/LoadingButton'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { withSnackbar } from 'material-ui-snackbar-provider'

const styles = (theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '70%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
})

class Login extends Component {
  constructor(props) {
    super(props)
    const { postData, status } = props
    this.state = {
      email: '',
      password: '',
      postData: postData,
      status: status,
    }
  }

  handleForm = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    })
  }

  submitForm = async () => {
    const { postData, status } = this.state
    return postData(this.state)
      .then((response) => {
        localStorage.setItem('status', status)
        if (status === 'COMPANY' && !response.optional.public_key) {
          this.props.history.push('/application-guide')
        } else {
          this.props.history.push('/dashboard')
        }
      })
      .catch((err) => {
        if (Array.isArray(err.error))
          this.props.snackbar.showMessage(err.error[0])
        else this.props.snackbar.showMessage(err.error)
      })
  }

  render() {
    const { classes } = this.props
    const { email, password } = this.state

    return (
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Log in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            value={email}
            onChange={this.handleForm}
            autoComplete="email"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            value={password}
            onChange={this.handleForm}
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <LoadingButton title="Log in" sendRequest={this.submitForm} />
          <Grid container>
            <Grid item xs>
              <Link href="/forgot" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="/landing/register" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    )
  }
}

export default withSnackbar()(withStyles(styles)(withRouter(Login)))
