import React, { Component } from 'react'
import Container from '@material-ui/core/Container'
import MaterialTable from 'material-table'
import { getAllSubmittedReportsForHacker } from '../../../shared/helpers/Request'

const columns = [
  { title: 'Report Name', field: 'name' },
  { title: 'Status', field: 'status' },
  { title: 'Bounty Name', field: 'bounty.name' },
]

class Submissions extends Component {
  constructor(props) {
    super(props)
    this.state = {
      reports: [],
    }
  }

  componentDidMount() {
    this.getData()
  }

  getData() {
    getAllSubmittedReportsForHacker().then((response) =>
      this.setState({ reports: response.reports })
    )
  }

  render() {
    const { reports } = this.state
    return (
      <Container
        maxWidth="md"
        style={{
          background: '#fff',
          height: '81vh',
          borderRadius: '25px',
          padding: '0px 25px 0px 25px',
          marginBottom: '8px',
          direction: 'column',
          alignItems: 'center',
          justifyItems: 'center',
        }}
        fixed
      >
        <MaterialTable
          title="Domains"
          columns={columns}
          data={reports}
          options={{
            actionsColumnIndex: -1,
          }}
        />
      </Container>
    )
  }
}

export default Submissions
