import React, { Component } from 'react'

import {
  Switch,
  Button,
  InputLabel,
  withStyles,
  FormControlLabel,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core'

import { withSnackbar } from 'material-ui-snackbar-provider'

import SettingsIcon from '@material-ui/icons/Settings'

import GridItem from '../../../Additional/GridItem'
import GridContainer from '../../../Additional/GridContainer'
import CustomInput from '../../../Additional/CustomInput'
import Card from '../../../Additional/Card'
import CardHeader from '../../../Additional/CardHeader'
import CardAvatar from '../../../Additional/CardAvatar'
import CardBody from '../../../Additional/CardBody'
import CardFooter from '../../../Additional/CardFooter'

import ROUTES from '../../../../shared/helpers/Constants'

const styles = {
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginTop: '0',
    marginBottom: '0',
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontSize: '20px',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
  },
  sectionInfo: {
    color: '#AAAAAA',
    marginTop: '25px',
  },
  description: {
    lineHeight: 1.5,
  },
  flexDiv: {
    display: 'flex',
  },
  setIcon: {
    margin: '0px 10px 0px 0px',
  },
  montSerrat: {
    fontFamily: "'Montserrat', sans-serif",
  },
  margin5: {
    margin: '5px',
  },
  justifyRight: {
    justifyContent: 'center',
  },
  currentInfo: {
    fontSize: '25px',
  },
  privAcc: {
    marginLeft: '-15px',
    fontWeight: '400',
    color: '#AAAAAA',
  },
}

class Hacker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hackerInfo: {},
      dialogOpen: false,

      first_name: '',
      last_name: '',
      username: '',
      home_address: '',
      voen: '',
      reputation: 0,
      oldPassword: '',
      newPassword: '',
      newPasswordConfirmation: '',
      avatar: '',
      is_private: false,
      status: '',
      bio: '',
      facebook: '',
      linkedin: '',
      instagram: '',
    }
    this.loadData = this.loadData.bind(this)
  }

  componentDidMount() {
    this.loadData()
  }

  loadData() {
    const { getData } = this.props
    getData().then((response) => {
      const hacker = response.hacker
      this.setState({
        first_name: hacker.first_name,
        last_name: hacker.last_name,
        username: hacker.username,
        home_address: hacker.home_address,
        voen: hacker.voen,
        bio: hacker.bio,
        status: hacker.status,
        facebook: hacker.facebook,
        linkedin: hacker.linkedin,
        instagram: hacker.instagram,
        is_private: hacker.is_private,
        avatar: hacker.avatar,
        hackerInfo: {
          username: hacker.username,
          first_name: hacker.first_name,
          last_name: hacker.last_name,
          status: hacker.status,
          nbReports: response.nbReports,
          reputation: hacker.reputation,
          bio: hacker.bio,
        },
      })
    })
  }

  setDialogOpen = () => {
    this.setState({
      dialogOpen: true,
    })
  }

  setDialogClose = () => {
    this.setState({
      dialogOpen: false,
    })
  }

  handleForm = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    })
  }

  handleSwitch = () => {
    this.setState({
      is_private: !this.state.is_private,
    })
  }

  submitForm = (event) => {
    event.preventDefault()
    const { putData } = this.props
    const requestBody = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      username: this.state.username,
      home_address: this.state.home_address,
      avatar: this.state.avatar,
      voen: this.state.voen,
      oldPassword: this.state.oldPassword,
      is_private: this.state.is_private,
      bio: this.state.bio,
      facebook: this.state.facebook,
      linkedin: this.state.linkedin,
      instagram: this.state.instagram,
    }

    putData(requestBody)
      .then(() => {
        this.props.snackbar.showMessage('Your profile updated successfully!')
        this.setDialogClose()
      })
      .catch((err) => {
        if (Array.isArray(err.error))
          this.props.snackbar.showMessage(err.error[0])
        else this.props.snackbar.showMessage(err.error)
      })
  }
  render() {
    const { classes } = this.props
    const {
      dialogOpen,
      first_name,
      last_name,
      username,
      home_address,
      avatar,
      voen,
      oldPassword,
      is_private,
      bio,
      facebook,
      linkedin,
      instagram,
    } = this.state

    const staticHackerInfo = this.state.hackerInfo
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={7}>
            <Card>
              <CardHeader color="danger">
                <div className={classes.flexDiv}>
                  <div className={classes.setIcon}>
                    <SettingsIcon fontSize="large" />
                  </div>
                  <div>
                    <h4 className={classes.cardTitleWhite}>Edit Profile</h4>
                    <p className={classes.cardCategoryWhite}>
                      Complete your profile
                    </p>
                  </div>
                </div>
              </CardHeader>
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <InputLabel className={classes.sectionInfo}>
                      ACCOUNT INFORMATION
                    </InputLabel>
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Username"
                      id="username"
                      inputProps={{
                        name: 'username',
                        value: username,
                        onChange: this.handleForm,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="New Password"
                      id="newPassword"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      inputProps={{
                        name: 'newPassword',
                        onChange: this.handleForm,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Re-enter password"
                      id="newPasswordConfirmation"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      inputProps={{
                        name: 'newPasswordConfirmation',
                        onChange: this.handleForm,
                      }}
                    />
                  </GridItem>
                </GridContainer>

                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <InputLabel className={classes.sectionInfo}>
                      PERSONAL INFORMATION
                    </InputLabel>
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                    <CustomInput
                      labelText="First Name"
                      id="first-name"
                      inputProps={{
                        name: 'first_name',
                        onChange: this.handleForm,
                        value: first_name,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <CustomInput
                      labelText="Last Name"
                      id="last-name"
                      inputProps={{
                        name: 'last_name',
                        onChange: this.handleForm,
                        value: last_name,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <CustomInput
                      labelText="Home Address"
                      id="home-address"
                      inputProps={{
                        name: 'home_address',
                        value: home_address,
                        onChange: this.handleForm,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <CustomInput
                      value={voen}
                      labelText="VOEN"
                      id="voen"
                      inputProps={{
                        name: 'voen',
                        value: voen,
                        onChange: this.handleForm,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                </GridContainer>

                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <InputLabel className={classes.sectionInfo}>
                      SOCIAL ACCOUNTS
                    </InputLabel>
                  </GridItem>
                </GridContainer>

                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      value={facebook}
                      labelText="Facebook"
                      id="facebook"
                      inputProps={{
                        name: 'facebook',
                        value: facebook,
                        onChange: this.handleForm,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="LinkedIn"
                      id="linkedin"
                      inputProps={{
                        name: 'linkedin',
                        value: linkedin,
                        onChange: this.handleForm,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Instagram"
                      id="instagram"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      inputProps={{
                        name: 'instagram',
                        value: instagram,
                        onChange: this.handleForm,
                      }}
                    />
                  </GridItem>
                </GridContainer>

                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <InputLabel className={classes.sectionInfo}>
                      ABOUT ME
                    </InputLabel>
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <CustomInput
                      labelText="Biography"
                      id="bio"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      inputProps={{
                        name: 'bio',
                        value: bio,
                        onChange: this.handleForm,
                        rows: 4,
                      }}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <FormControlLabel
                      control={
                        <Switch
                          color="secondary"
                          checked={is_private}
                          name="is_private"
                          onChange={this.handleSwitch}
                        />
                      }
                      label={
                        <span className={classes.privAcc}>PRIVATE ACCOUNT</span>
                      }
                      labelPlacement="start"
                    />
                  </GridItem>
                </GridContainer>
              </CardBody>
              <CardFooter>
                <Button
                  variant="outlined"
                  color="secondary"
                  onClick={this.setDialogOpen}
                >
                  Save & Update Profile
                </Button>
                <Dialog
                  open={dialogOpen}
                  onClose={this.setDialogClose}
                  aria-labelledby="form-dialog-title"
                >
                  <DialogTitle>Profile Update</DialogTitle>
                  <DialogContent>
                    <DialogContentText>
                      To save changes and update your profile, please enter your
                      password.
                    </DialogContentText>
                    <TextField
                      color="secondary"
                      autoFocus
                      margin="dense"
                      id="oldPassword"
                      label="Your Password"
                      type="password"
                      name="oldPassword"
                      value={oldPassword}
                      onChange={this.handleForm}
                      fullWidth
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button
                      onClick={this.setDialogClose}
                      color="secondary"
                      variant="outlined"
                    >
                      Cancel
                    </Button>
                    <Button
                      onClick={this.submitForm}
                      color="secondary"
                      variant="contained"
                    >
                      Update
                    </Button>
                  </DialogActions>
                </Dialog>
              </CardFooter>
            </Card>
          </GridItem>

          <GridItem xs={12} sm={12} md={5}>
            <Card profile>
              <CardAvatar profile>
                <a
                  href={`${ROUTES.devURL}/image/avatar/${avatar}`}
                  onClick={(e) => e.preventDefault()}
                >
                  <img
                    src={`${ROUTES.devURL}/image/avatar/${avatar}`}
                    alt={username}
                  />
                </a>
              </CardAvatar>
              <CardBody profile>
                <h3
                  style={{ fontWeight: '550', color: '#716b68' }}
                  className={classes.cardCategory}
                >
                  {`${staticHackerInfo.username} | ${staticHackerInfo.status}`}
                </h3>
                <h1
                  className={classes.cardTitle}
                >{`${staticHackerInfo.first_name} ${staticHackerInfo.last_name} •	 ${staticHackerInfo.reputation}`}</h1>
                <p
                  className={classes.description}
                >{`${staticHackerInfo.bio}`}</p>
                <Button variant="contained" color="secondary">
                  {`SUBMITTED REPORTS (${staticHackerInfo.nbReports})`}
                </Button>
              </CardBody>
            </Card>

            {/* <Card profile>
              <GridContainer style={{ marginTop: '18px' }}>
                <GridItem xs={12} sm={12} md={12}>
                  <Typography
                    variant="subtitle2"
                    className={classes.currentInfo}
                  >
                    Current Profile Info
                  </Typography>
                </GridItem>
              </GridContainer>

              <GridContainer>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText={`${hacker['username']} (Username)`}
                    id="username"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      disabled: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText={`${hacker['email']} (E-mail Address)`}
                    id="email"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      disabled: true,
                    }}
                  />
                </GridItem>
              </GridContainer>

              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText={`${hacker['voen']} (VOEN)`}
                    id="voen"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      disabled: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={8}>
                  <CustomInput
                    labelText={`${hacker['home_address']} (Home Address)`}
                    id="home"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      disabled: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <CardFooter className={classes.justifyRight}>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href={`${hacker['facebook']}`}
                      target="_blank"
                    >
                      <FacebookIcon fontsize="small" />
                    </Button>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href={`${hacker['instagram']}`}
                      target="_blank"
                    >
                      <InstagramIcon fontsize="small" />
                    </Button>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href={`${hacker['linkedin']}`}
                      target="_blank"
                    >
                      <LinkedInIcon fontsize="small" />
                    </Button>
                  </CardFooter>
                </GridItem>
              </GridContainer>
            </Card> */}
          </GridItem>
        </GridContainer>
      </div>
    )
  }
}

export default withSnackbar()(withStyles(styles)(Hacker))
