import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'

import { getHackerProfile } from '../../../shared/helpers/Request'

import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'

import FacebookIcon from '@material-ui/icons/Facebook'
import InstagramIcon from '@material-ui/icons/Instagram'
import LinkedInIcon from '@material-ui/icons/LinkedIn'
import ThumbUpIcon from '@material-ui/icons/ThumbUp'

const styles = {
  root: {
    width: '100%',
    marginTop: '50px',
    textAlign: 'center',
    '& img': {
      maxWidth: '160px',
      borderRadius: '50%',
      width: '100%',
      margin: '0 auto',
      transform: 'translate3d(0, -50%, 0)',
    },
  },
  name: {
    marginTop: '-80px',
  },
  title: {
    color: 'steelblue',
    margin: '1.75rem 0 -0.875rem',
    fontWeight: '700',
    display: 'inline-block',
    position: 'relative',
    marginTop: '30px',
    minHeight: '32px',
  },
  subtext: {
    color: '#1b7a1d',
  },
  reputation: {
    display: 'inline',
    padding: '10px',
    color: '#c3073f',
  },
}

class HackerProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: props.match.params.id,
      first_name: '',
      last_name: '',
      username: '',
      avatar: '',
      status: '',
      reputation: 0,
      facebook: '',
      linkedin: '',
      instagram: '',
    }
    this.loadData = this.loadData.bind(this)
  }

  componentDidMount() {
    this.loadData()
  }

  loadData() {
    getHackerProfile({ id: this.state.id }).then((response) => {
      this.setState({
        first_name: response.hacker.first_name,
        last_name: response.hacker.last_name,
        username: response.hacker.username,
        avatar: response.hacker.avatar,
        status: response.hacker.status,
        reputation: response.hacker.reputation,
        facebook: response.hacker.facebook,
        linkedin: response.hacker.linkedin,
        instagram: response.hacker.instagram,
      })
    })
  }

  // will be img src={this.state} when images are available
  render() {
    const { classes } = this.props
    return (
      <Paper className={classes.root}>
        <img
          src="https://image.freepik.com/free-vector/hacker-activity-concept-with-man-illustration_23-2148530633.jpg"
          alt={this.state.username}
        />
        <div className={classes.name}>
          <h3 className={classes.title}>
            {this.state.first_name} {this.state.last_name}
          </h3>
          <h5 className={classes.subtext}>
            {this.state.username} | {this.state.status}
          </h5>
          <span className={classes.reputation}>
            <ThumbUpIcon />{' '}
            <h3 style={{ display: 'inline' }}>
              Reputation : {this.state.reputation}
            </h3>
          </span>
        </div>
        <hr />
        <Button>
          <a href={this.state.facebook}>
            <FacebookIcon />
          </a>
        </Button>
        <Button>
          <a href={this.state.instagram}>
            <InstagramIcon />
          </a>
        </Button>
        <Button>
          <a href={this.state.linkedin}>
            <LinkedInIcon />
          </a>
        </Button>
      </Paper>
    )
  }
}

export default withStyles(styles)(HackerProfile)
