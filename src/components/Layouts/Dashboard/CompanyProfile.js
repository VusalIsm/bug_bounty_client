import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'

import { getCompanyProfile } from '../../../shared/helpers/Request'

import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListSubheader from '@material-ui/core/ListSubheader'

import FacebookIcon from '@material-ui/icons/Facebook'
import InstagramIcon from '@material-ui/icons/Instagram'
import LinkedInIcon from '@material-ui/icons/LinkedIn'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import PhoneIcon from '@material-ui/icons/Phone'
import MailIcon from '@material-ui/icons/Mail'

const styles = {
  info: {
    width: '100%',
    height: '230px',
    marginTop: '130px',
    '& img': {
      maxWidth: '230px',
      float: 'left',
      width: '100%',
      margin: '0 auto',
    },
  },
  name: {
    marginTop: '-80px',
  },
  title: {
    color: 'steelblue',
    margin: '1.75rem 0 -0.875rem',
    fontWeight: '700',
    display: 'inline-block',
    position: 'relative',
    marginTop: '30px',
    marginLeft: '15px',
    marginBottom: '5px',
    minHeight: '32px',
  },
  subtext: {
    color: '#c3073f',
    marginLeft: '15px',
    display: 'inline-block',
  },
  reputation: {
    display: 'inline',
    padding: '10px',
  },
  bounty: {
    marginTop: '10px',
  },
}

class CompanyProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: props.match.params.id,
      name: '',
      email: '',
      address: '',
      logo: '',
      description: 'Here will be description about company',
      contact_number: '',
      domains: ['domain1.company.com', 'domain2.company.com'],
      bounties: [
        {
          _id: 1,
          name: 'Bounty 1',
          low_price: 100,
          high_price: 500,
          currency: 'AZN',
        },
        {
          _id: 2,
          name: 'Bounty 2',
          low_price: 120,
          high_price: 400,
          currency: 'USD',
        },
        {
          _id: 3,
          name: 'Bounty 3',
          low_price: 150,
          high_price: 450,
          currency: 'AZN',
        },
      ],
      facebook: '',
      linkedin: '',
      instagram: '',
    }
    this.loadData = this.loadData.bind(this)
  }

  componentDidMount() {
    this.loadData()
  }

  loadData() {
    getCompanyProfile({ id: this.state.id }).then((response) => {
      // after modifying the back-end, these fields will be uncommented
      this.setState({
        name: response.company.name,
        email: response.company.email,
        address: response.company.address,
        logo: response.company.logo,
        // description: response.company.description,
        contact_number: response.company.contact_number,
        // domains: response.company.domains,
        // bounties: response.company.bounties,
        facebook: response.company.facebook,
        linkedin: response.company.linkedin,
        instagram: response.company.instagram,
      })
    })
  }

  render() {
    const { classes } = this.props

    const bounties = this.state.bounties.map((bounty) => {
      return (
        <ListItem key={bounty._id} button component="a" href="#bounty">
          <ListItemText>{bounty.name}</ListItemText>
          <ListSubheader>
            {bounty.low_price} - {bounty.high_price} {bounty.currency}
          </ListSubheader>
        </ListItem>
      )
    })

    // will be img src={this.state} when images are available
    return (
      <React.Fragment>
        <Paper className={classes.info}>
          <div className={classes.profile}>
            <img
              src="https://seeklogo.com/images/Y/youtube-square-logo-3F9D037665-seeklogo.com.png"
              alt={this.state.name}
            />
            <div className={classes.name}>
              <h1 className={classes.title}>{this.state.name}</h1>
              <br />
              <div>
                <span className={classes.reputation}>
                  <LocationOnIcon /> {this.state.address}
                </span>
                <span className={classes.reputation}>
                  <PhoneIcon /> {this.state.contact_number}
                </span>
                <span className={classes.reputation}>
                  <MailIcon /> {this.state.email}
                </span>
              </div>

              <h3 className={classes.subtext}>{this.state.description}</h3>
            </div>
            <hr />
            <Button>
              <a href={this.state.facebook}>
                <FacebookIcon />
              </a>
            </Button>
            <Button>
              <a href={this.state.instagram}>
                <InstagramIcon />
              </a>
            </Button>
            <Button>
              <a href={this.state.linkedin}>
                <LinkedInIcon />
              </a>
            </Button>
          </div>
        </Paper>
        <Paper className={classes.bounty}>
          <h3 className={classes.title}>Domains:</h3>
          <br />
          <h4 className={classes.subtext}>{this.state.domains.join(', ')}</h4>
          <br />
          <h3 className={classes.title}>Bounties:</h3>
          <br />
          <List aria-label="secondary">{bounties}</List>
        </Paper>
      </React.Fragment>
    )
  }
}

export default withStyles(styles)(CompanyProfile)
