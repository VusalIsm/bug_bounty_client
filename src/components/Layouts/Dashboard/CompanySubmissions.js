import React, { Component } from 'react'
import Container from '@material-ui/core/Container'

import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import MaterialTable from 'material-table'
import { decryptData } from '../../../shared/helpers/RSA'
import { downloadMessageAsPlainText } from '../../../shared/helpers/File'
import { getAllSubmittedReportsForCompany } from '../../../shared/helpers/Request'

export default class CompanySubmissions extends Component {
  constructor(props) {
    super(props)
    this.state = {
      reports: [],
      isOpen: false,
      currentReport: null,
      privateKey: '',
    }
    this.downloadReport = this.downloadReport.bind(this)
  }
  componentDidMount() {
    this.getData()
  }

  async downloadReport() {
    const { privateKey, currentReport } = this.state
    const decryptedMessage = await decryptData(
      privateKey,
      currentReport.content
    ).catch((err) => console.log(err))
    downloadMessageAsPlainText(decryptedMessage, `Report.txt`)
  }

  getData() {
    getAllSubmittedReportsForCompany().then((response) =>
      this.setState({ reports: response.reports })
    )
  }

  handleClickOpen = (data) => {
    this.setState({ isOpen: true, currentReport: data })
  }

  handleClose = () => {
    this.setState({ isOpen: false })
  }

  showFile = async (e) => {
    e.preventDefault()
    const reader = new FileReader()
    const privateKey = ''
    reader.onload = async (e) => {
      this.setState({ privateKey: e.target.result })
    }
    reader.readAsText(e.target.files[0])
  }

  render() {
    const { reports, isOpen } = this.state

    return (
      <div>
        <MaterialTable
          title="Positioning Actions Column Preview"
          columns={[
            { title: 'Report Name', field: 'name' },
            { title: 'Bounty Name', field: 'bounty.name' },
            { title: 'Status', field: 'status' },
          ]}
          data={reports}
          actions={[
            {
              icon: 'save',
              tooltip: 'Download Report',
              onClick: (event, rowData) => this.handleClickOpen(rowData),
            },
          ]}
          options={{
            actionsColumnIndex: -1,
          }}
        />
        <Dialog
          open={isOpen}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Add Private Key</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Please upload your private key here.
            </DialogContentText>
            <Button variant="contained" component="label">
              Upload File
              <input
                type="file"
                style={{ display: 'none' }}
                onChange={(e) => this.showFile(e)}
              />
            </Button>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.downloadReport} color="primary">
              Download Report
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
