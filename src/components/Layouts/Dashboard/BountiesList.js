import React, { Component } from 'react'

import Container from '@material-ui/core/Container'

import ServerPaginationTable from '../../Tables/ServerPaginationTable'
import { getBounties } from '../../../shared/helpers/Request'
import Bounty from '../../Tables/Rows/Bounty'

class BountiesList extends Component {
  render() {
    return (
      <Container
        maxWidth="md"
        style={{
          background: '#fff',
          minHeight: '81vh',
          borderRadius: '25px',
          padding: '25px',
          marginBottom: '8px',
        }}
        fixed
      >
        <ServerPaginationTable getData={getBounties} rowsPerPage={6}>
          <Bounty />
        </ServerPaginationTable>
      </Container>
    )
  }
}

export default BountiesList
