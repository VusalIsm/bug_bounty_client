import React, { Component } from 'react'

import { withStyles } from '@material-ui/styles'
import Container from '@material-ui/core/Container'
import { Typography } from '@material-ui/core'

import './PolicyContent.css'

const styles = {
  root: {},
  container: {},
  mainHeading: {
    textTransform: 'uppercase',
    fontSize: '60px',
    fontWeight: 'bold',
    marginBottom: '50px',
  },
  heading: {
    textTransform: 'uppercase',
    fontSize: '20px',
    fontWeight: 'bold',
    paddingBottom: '100px',
  },
  body: {
    fontFamily: '"Montserrat", sans-serif',
  },
}

class PolicyContent extends Component {
  render() {
    const { classes } = this.props
    return (
      <Container className={classes.container} maxWidth="lg">
        <Typography className={classes.mainHeading} variant="h1">
          Website Policy and Disclosure Guidelines
        </Typography>
        <Typography variant="button" className={classes.heading}>
          What is Bug Bounty Program ?
        </Typography>
        <br />
        <br />
        <Typography variant="body1" className={classes.body}>
          A ​ <span class="boldify">bug bounty program​</span> is a deal offered
          by many websites, organizations and software developers by which
          individuals can receive recognition and compensation​ for reporting ​
          bugs​ , especially those pertaining to security ​ exploits​ and ​
          vulnerabilities​ . These programs allow the developers to discover and
          resolve bugs before the general public is aware of them, preventing
          incidents of widespread abuse. Bug bounty programs have been
          implemented by a large number of organizations, including ​
          <span class="boldify">
            Mozilla​ , ​ Facebook​ , ​ ​ Yahoo!​ , ​ Google​ , ​ Reddit​ , ​
            Square​ , ​ Microsoft​ and the Internet bug bounty
          </span>{' '}
          .
        </Typography>
        <br />
        <br />
        <br />
        <Typography variant="button" className={classes.heading}>
          Vulnerability Disclosure Policy & Guidelines
        </Typography>
        <br />
        <br />
        <br />
        <Typography variant="button" className={classes.heading}>
          Definitions
        </Typography>
        <br />
        <br />
        <Typography variant="body1" className={classes.body}>
          The terms <span class="boldify">“us” or “we” or “our team”</span>{' '}
          refers to ​{' '}
          <a class="special-link" href="/landing/#here-is-our-team">
            Team SecUrZone​
          </a>
          , the owner of the website.
          <br />
          <br />
          <br />A ​<span class="boldify">“Hacker”</span> ​ means an individual
          or entity using the SecUrZone Platform to provide hacker submissions.{' '}
          <br />
          <br />A ​<span class="boldify">“Hacker Submission”</span>​ means
          documents and related materials evidencing a Hacker’s activities
          related to a Bug Bounty Program including Vulnerability Reports.
          <br />
          <br />
          A​ <span class="boldify">“Vulnerability Report”​</span> means bug
          reports or other vulnerability information, in text, graphics, image,
          software, works of authorship of any kind, and information or other
          material that Hackers provide or otherwise make available through the
          SecUrZone Platform to a Company resulting from participation in a
          Program.
          <br />
          <br /> A​ <span class="boldify">“Program”</span>​ means the security
          initiative(s) for which a Company desires to receive Hacker
          Submissions from Hackers, which a Company posts to the SecUrZone
          Platform.
          <br />
          <br /> A <span class="boldify">“Company” </span>​ is a large
          organization or company that has different bug bounty or in other
          words, vulnerability disclosure p rograms. <br />
          <br />A <span class="boldify">“Responsible”</span> ​ being a
          Company-selected person (it may be a worker in any position, generally
          IT Staff), runs the Company’s account on behalf of the Company,
          manages active programs and controls submitted reports directly.
          <br />
          <br /> A <span class="boldify">“Member”​</span> is an individual that
          has registered with us to use our Service.Can be both Hacker and
          Responsible.
          <br />
          <br /> A ​<span class="boldify">“Visitor”</span>​ is someone who
          merely browses our Website but has not registered as a Member. <br />
          <br />A ​ <span class="boldify">“Program Policy”</span> ​ includes a
          Company created description of the security-related and other services
          that the Company is seeking from Hackers, the terms, conditions, and
          requirements governing the Program to which the Hackers must agree,
          and the Rewards, if any, that a Company will award to Hackers who
          participate in the Program.
        </Typography>
        <br />
        <br />
        <br />
        <br />
        <Typography variant="button" className={classes.heading}>
          Registration process for Hackers
        </Typography>
        <br />
        <br />
        <Typography variant="body1" className={classes.body}>
          Hackers joining our platform should be over 18 in order to start
          finding vulnerabilities and get rewards.During the registration
          process meant for hackers, contrary to what many platforms want, we
          also take Hacker’s VOEN (​ Azerbaijan ​ Republic ​ Taxpayer ​
          Identification ​ Number) as it is the most suitable way for us to
          verify him/her (person who is registering) as juridical person.
          <span class="boldify">
            We do not share or disclose your VOEN (or ARTIN) with anybody else​,
          </span>
          even with Companies, unless we have been reported any abuse from you
          or your account (e.g finding vulnerabilities on unauthorized domains).
        </Typography>
        <br />
        <br />
        <br />
        <br />
        <Typography variant="button" className={classes.heading}>
          What is VOEN (ARTIN) number?
        </Typography>
        <br />
        <br />
        <Typography variant="body1" className={classes.body}>
          According to the decision of the Cabinet of Ministers of the
          Azerbaijan Republic dated December 21, 2000, registration of all legal
          entities, their branches and representative offices, as well as
          individuals (individual entrepreneurs) who are taxpayers, regardless
          of the form of ownership, are registered by the state tax authorities
          of the Azerbaijan Republic with Taxpayer Identification Number.
          Identification numbers of taxpayers registered before January 1, 2001
          shall consist of identification numbers (TIN) previously issued to
          them, and the notifications submitted shall be equal to the
          certificates confirming tax registration.
        </Typography>
        <br />
        <br />
        <br />
        <Typography variant="body1" className={classes.body}>
          After the registration process, you shall have to verify your email by
          the token that will be sent to your email address right after you
          click the “Register” button.Sometimes emails can take more than 5
          minutes to reach out.
          <br />
          <br />
          Once you verified your account, your Hacker status will be set to
          <span class="boldify italify">“HACKER”</span> and no longer{' '}
          <span class="boldify italify">“PENDING”</span>.Only Hackers with a
          status set as “HACKER” can log into their accounts and start using our
          Services.
          <br />
          <br />
          Hackers can change their profile status to private afterwards if they
          want, Hacker’s profile page and the other information belonging to the
          Hacker, (<span class="italify">not counting username</span>) will not
          be displayed if a profile is set to private.
        </Typography>
        <br />
        <br />
        <br />
        <br />
        <Typography variant="button" className={classes.heading}>
          Registration Process for Companies / Responsibles
        </Typography>
        <br />
        <br />
        <Typography variant="body1" className={classes.body}>
          To register and act as a Company in our platform, a responsible should
          choose “For Companies” in the registration page and continue filling
          up the information referring both to Responsible himself and to the
          Company. Two separate emails are required to register as a company.One
          email belonging to the Responsible (this cannot be Responsible’s
          personal email address) in which the verification process will be held
          and the other belonging to the Company.Responsible’s email won’t be
          displayed afterwards and is only needed logging into Company’s
          account.
          <br />
          <br />
          Responsibles should be chosen wisely as they will be responsible after
          the registration process.Responsible will have direct access to all
          Vulnerability Reports submitted to a Company’s special Program or
          Programs. They can manage Programs, edit Program Policies and mark
          submitted Vulnerability Reports correspondingly.
          <br />
          <br />
          Responsibles should also add ​{' '}
          <span class="boldify italify">Low, Medium and Critical</span> ​ (High)
          Bounty amounts and choose the corresponding currency after the
          registration and verification process is done.{' '}
          <span class="boldify">
            It’s well worth to mention that these 3 different level bounty
            amounts are applied to all Programs of a Company and not the
            specific one.
          </span>
        </Typography>
        <br />
        <br />
        <br />
        <br />
        <Typography variant="button" className={classes.heading}>
          Opening (Joining) a Bug Bounty Program
        </Typography>
        <br />
        <br />
        <Typography variant="body1" className={classes.body}>
          Creating a new bug bounty program for a Company is a simple process
          that can only be done by Responsibles. Each Program requires a fully
          comprehensive policy as Hackers could misunderstand the policy and
          irreversible changes could happen.So make sure you as Responsibles
          provide enough information for Hackers and touch every point in the
          Program Policy section belonging to each Program.We remind you that a
          Company can have <span class="boldify italify">many</span>​ Programs.
          <br />
          <br />
          Although the process does not finish here.As created Program will have
          at least one domain mentioned in the Program Policy for Hackers to be
          able to discover vulnerabilities on, Responsibles have to verify the
          domain(s) first using domain verifier tool of SecUrZone.Note that ​
          <span class="boldify">only</span> ​ verified domains can be added to
          the Program Policy section.
        </Typography>
        <br />
        <br />
        <br />
        <br />
        <Typography variant="button" className={classes.heading}>
          The Submission Process
        </Typography>
        <br />
        <br />
        <Typography variant="body1" className={classes.body}>
          If you believe you have discovered a vulnerability, please create a
          submission for the appropriate program through the Company’s profile
          page. Each program has a set of guidelines called the Program Policy.
          The Program Policy is maintained by the Responsible. Terms specified
          in the Program Policy supersede (or override) these terms.
          <br />
          <br />
          Each submission will be updated with significant events, including
          when the issue has been validated, when we need more information from
          you, or when you have qualified for a reward.
          <br />
          <br />
          Each submission is evaluated by the Responsible on the basis of
          first-to-find. SecUrZone may assist in the evaluation process.
          <br />
          <br />
          You will qualify for a reward if you were the first person to alert
          the Responsible to a previously unknown issue AND the issue triggers a
          code or configuration change.
        </Typography>
        <br />
        <br />
        <br />
        <br />
        <Typography variant="button" className={classes.heading}>
          Standard Program Rules
        </Typography>
        <br />
        <br />
        <Typography variant="body1" className={classes.body}>
          We are committed to protecting the interests of Security Researchers
          (Hackers). The more closely your behavior follows these rules, the
          more we’ll be able to protect you if a difficult situation escalates.
          <br />
          <br />
          Rules can vary for each program. Please carefully read the Program
          Policy for specific rules. These rules apply to ​{' '}
          <span class="boldify upperify">ALL PROGRAMS​ :</span>
        </Typography>
        <br />
        <ul>
          <li>
            Testing should be performed only on systems or domains listed under
            the Program Policy “Domains” section. Any other domains or systems
            are Out Of Scope.
          </li>
          <br />
          <li>
            Except when otherwise noted in the Program Policy, you should create
            accounts for testing purposes.
          </li>
          <br />
          <li>
            Submissions must be made exclusively through SecUrZone to be
            considered for a reward.
          </li>
          <br />
          <li>
            Communication regarding submissions must be held out of SecUrZone
            Platform and directly with the Responsible / Company.
          </li>
          <br />
          <li>
            Actions which affect the integrity or availability of program
            targets (domains) are prohibited and strictly enforced. If you
            notice performance degradation on the target systems, you must
            immediately suspend all use of automated tools.
          </li>
          <br />
          <li>
            Submissions may be closed if a Hacker is non-responsive to requests
            for information after 15 days.
          </li>
          <br />
          <li>
            The existence or details of Programs that are allowed to PRO
            Verified Hackers must not be communicated to anyone who is not a
            SecUrZone employee or Responsible.
          </li>
          <br />
          <li>
            We encourage Hackers to include a video or screenshot
            Proof-of-Concept in their submissions. These files should not be
            shared publicly. This includes uploading to any publicly accessible
            websites (i.e. YouTube, DailyMotion etc.). If the file exceeds
            100MB, upload the file to a secure online service such as Google
            Drive, with a shared link.
          </li>
          <br />
          <li>
            SecUrZone’s Disclosure policies apply to all submissions made
            through the SecUrZone platform, including Duplicates, Out of Scope,
            and Not Applicable submissions.
          </li>
          <br />
          <li>
            If a Hacker wants to retain disclosure rights for vulnerabilities
            that are out of scope for a bounty program, they should report the
            issue to the Responsible directly.SecUrZone can assist Hackers in
            identifying the appropriate email address to contact. Responsibles
            are encouraged to ensure their program scope includes all critical
            components they wish to receive vulnerability reports for.
          </li>
          <br />
          <li>
            Violation of a Program’s stated disclosure policy may result in
            enforcement action as mentioned in the SecUrZone Vulnerability
            Disclosure Policy & Guidelines.
          </li>
          <br />
          <li>
            You must be at least 18 years old or have reached the age of
            majority in your jurisdiction of primary residence and citizenship
            to be eligible to receive any monetary compensation as a Hacker.
          </li>
          <br />
          <li>
            Normally, the qualification process for a reward is held between the
            Hacker and Responsible (Company) itself, but in case if you have
            taken away your rights (or at least if you think you have), Team
            SecUrZone will intervene and make everything clear for both sides.
          </li>
        </ul>
        <br />
        <br />
        <br />
        <br />
        <Typography variant="button" className={classes.heading}>
          Passwords and Security
        </Typography>
        <br />
        <br />
        <Typography variant="body1" className={classes.body}>
          You may need to set up an account in order to use some of the features
          of the Website. You may not use a third party’s account without
          permission. When you are setting up your account, you must give us
          accurate and complete information. This means that you cannot set up
          an account using a name or contact information that does not apply to
          you, and you must provide accurate and current information on all
          registration forms that are part of the Website. You may only set up
          one account. You have complete responsibility for your account and
          everything that happens on your account. This means you need to be
          careful with your password. If you find out that someone is using your
          account without your permission, you must let us know immediately. You
          may not transfer your account to someone else. We are not liable for
          any damages or losses caused by someone using your account without
          your permission. However, if we (or anyone else) suffer any damage due
          to the unauthorized use of your account, you may be liable.
        </Typography>
        <br />
        <br />
      </Container>
    )
  }
}

export default withStyles(styles)(PolicyContent)
