import React, { Fragment } from 'react'
import { ReactComponent as LandingAnim } from '../../../animations/landing-anim.svg'
import CountUp from 'react-countup'
import PropTypes from 'prop-types'
import { Typography, Box, Grid, makeStyles, useTheme } from '@material-ui/core'
import { AppBar, Tabs, Tab } from '@material-ui/core'
import '../../../animations/landing-anim-style.css'
import './LandingContent.css'
import TransparentButton from '../../Buttons/TransparentButton'
import SwipeableViews from 'react-swipeable-views'
import CompaniesSection from './Sections/CompaniesSection'
import HackersSection from './Sections/HackersSection'
import TeamSection from './Sections/TeamSection'
import ContactSection from './Sections/ContactSection'
import Footer from '../../Footer/Footer'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  tab_root: {
    // backgroundColor: theme.palette.background.paper,
    backgroundColor: 'transparent',
    // width: '1920px',
  },
}))

function TabPanel(props) {
  const { children, value, index } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
}

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  }
}

export default function LandingContent() {
  const classes = useStyles()
  const theme = useTheme()
  const [value, setValue] = React.useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const handleChangeIndex = (index) => {
    setValue(index)
  }
  return (
    <Fragment>
      <div style={{ padding: 20, height: '70vh' }}>
        <div className={classes.root}>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <LandingAnim />
            </Grid>
            <Grid
              item
              xs={6}
              className={classes.title}
              style={{ color: 'white', padding: '10vw' }}
            >
              <h1>Hack securely without having any limitations.</h1>
              <p style={{ lineHeight: 1.5 }}>
                The first bug bounty platform ever created in Azerbaijan. Go
                beyond vulnerability scanners and penetration tests with trusted
                security expertise that scales — and find critical issues faster
                without having any limitations by hacking for good.
              </p>
              <br />
              <TransparentButton
                btnstyle="get-started"
                baseUrl="/landing/register"
                title="Get Started"
              />
              <TransparentButton
                btnstyle="learn-more"
                baseUrl="/landing"
                anchor="how-it-works"
                title="Learn More"
              />
            </Grid>
          </Grid>
        </div>
      </div>
      <div className="gradient">
        <Grid
          className="grid"
          container
          direction="row"
          justify="space-evenly"
          alignItems="center"
        >
          <Grid className="counter" item>
            <CountUp end={10} duration="5" />
            <p>hackers</p>
          </Grid>
          <Grid className="counter" item>
            <CountUp end={80} duration="5" />
            <p>issues are reported</p>
          </Grid>
          <Grid className="counter" item>
            <CountUp end={100} duration="5" />
            <p>safe</p>
          </Grid>
        </Grid>
      </div>
      <div>
        <AppBar position="static" color="transparent">
          <Tabs
            className={classes.tab_root}
            value={value}
            onChange={handleChange}
            indicatorColor="secondary"
            textColor="secondary"
            variant="fullWidth"
          >
            <Tab label="For Hackers" {...a11yProps(0)} />
            <Tab label="For Companies" {...a11yProps(1)} />
          </Tabs>
        </AppBar>
        <SwipeableViews
          id="how-it-works"
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={value}
          onChangeIndex={handleChangeIndex}
        >
          <TabPanel value={value} index={0} dir={theme.direction}>
            <HackersSection />
          </TabPanel>
          <TabPanel value={value} index={1} dir={theme.direction}>
            <CompaniesSection />
          </TabPanel>
        </SwipeableViews>
      </div>

      <div>
        <TeamSection />
      </div>
      <div>
        <ContactSection />
      </div>
      <Footer />
    </Fragment>
  )
}
