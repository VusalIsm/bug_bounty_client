import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import CreateIcon from '@material-ui/icons/Create'
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn'
import AccessTimeIcon from '@material-ui/icons/AccessTime'
import LockOpenIcon from '@material-ui/icons/LockOpen'
import KeyboardIcon from '@material-ui/icons/Keyboard'
import FindInPageIcon from '@material-ui/icons/FindInPage'

import GridContainer from '../../../Additional/GridContainer'
import GridItem from '../../../Additional/GridItem'
import InfoArea from '../../../Additional/InfoArea'

const title = {
  color: '#FFFFFF',
  margin: '1.75rem 0 0.875rem',
  textDecoration: 'none',
  fontWeight: '2000',
  fontFamily: `"Montserrat", sans-serif`,
}

const productStyle = {
  section: {
    backgroundColor: '#292929',
    padding: '20px 200px',
    textAlign: 'center',
    color: '#F5F0F0',
  },
  title: {
    ...title,
    marginBottom: '1rem',
    // marginTop: "30px",
    minHeight: '32px',
    textDecoration: 'none',
  },
  description: {
    // color: "#F5F0F0"
  },
}

const useStyles = makeStyles(productStyle)

export default function ProductSection() {
  const classes = useStyles()
  return (
    <div className={classes.section}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <h2 style={{ fontSize: '3rem' }} className={classes.title}>
            How it works
          </h2>
          <h3 className={classes.description}>
            Learn more by following the below instructions on getting started in
            our bug bounty platform. Being easy-to-use and totally free, our
            platform offers a number of options to keep your company secure &
            safe.
          </h3>
        </GridItem>
      </GridContainer>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Register an account"
              description="If you are ready to chase some bugs and also contribute to big companies — register a new account immediately"
              icon={CreateIcon}
              iconColor="info"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Search Programs"
              description="Poke around and look for some bug bounty programs you think you can hack and choose one to get started"
              icon={FindInPageIcon}
              iconColor="success"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Get to work"
              description="Start hacking or testing the target domains but make sure you have read & acknowledged its policy and you do the things as should be"
              icon={KeyboardIcon}
              iconColor="warning"
              vertical
            />
          </GridItem>
        </GridContainer>
      </div>
      <div style={{ paddingTop: '70px' }}>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="If you're in..."
              description="After finding a vulnerability in the target application, make the best report fit for yourself and submit it if it's worth to mention about"
              icon={LockOpenIcon}
              iconColor="gray"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="This will take some time"
              description="Usually companies report back in short time.We make sure they have quite capable responsibles to operate on behalf of them"
              icon={AccessTimeIcon}
              iconColor="rose"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Claim what's yours"
              description="The payment process will be held when your report marked as 'approved' by the company.If you think you lost your rights, contact us immediately"
              icon={MonetizationOnIcon}
              iconColor="danger"
              vertical
            />
          </GridItem>
        </GridContainer>
      </div>
    </div>
  )
}
