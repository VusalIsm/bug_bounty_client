import React from 'react'
import classNames from 'classnames'
import { makeStyles } from '@material-ui/core/styles'
import GridContainer from '../../../Additional/GridContainer.js'
import GridItem from '../../../Additional/GridItem.js'
import Button from '@material-ui/core/Button'
import Card from '../../../Additional/Card.js'
import CardBody from '../../../Additional/CardBody.js'
import CardFooter from '../../../Additional/CardFooter.js'

import FacebookIcon from '@material-ui/icons/Facebook'
import LinkedInIcon from '@material-ui/icons/LinkedIn'
import GitHubIcon from '@material-ui/icons/GitHub'

import Vusal from '../../../Additional/team/Vusal.jpeg'
import Fahmin from '../../../Additional/team/Fahmin.png'
import Saida from '../../../Additional/team/Saida.png'
import Vusala from '../../../Additional/team/Vusala.png'
import Nihat from '../../../Additional/team/Nihat.png'

const title = {
  color: '#3C4858',
  margin: '1.75rem 0 0.875rem',
  textDecoration: 'none',
  fontWeight: '700',
  fontFamily: `"Montserrat", sans-serif`,
  fontSize: '1rem',
}

const cardTitle = {
  ...title,
  marginTop: '.625rem',
}

const imagesStyle = {
  imgFluid: {
    maxWidth: '100%',
    height: 'auto',
  },
  imgRounded: {
    borderRadius: '6px !important',
  },
  imgRoundedCircle: {
    borderRadius: '50% !important',
  },
  imgRaised: {
    boxShadow:
      '0 5px 15px -8px rgba(0, 0, 0, 0.24), 0 8px 10px -5px rgba(0, 0, 0, 0.2)',
  },
  imgGallery: {
    width: '100%',
    marginBottom: '2.142rem',
  },
  imgCardTop: {
    width: '100%',
    borderTopLeftRadius: 'calc(.25rem - 1px)',
    borderTopRightRadius: 'calc(.25rem - 1px)',
  },
  imgCardBottom: {
    width: '100%',
    borderBottomLeftRadius: 'calc(.25rem - 1px)',
    borderBottomRightRadius: 'calc(.25rem - 1px)',
  },
  imgCard: {
    width: '100%',
    borderRadius: 'calc(.25rem - 1px)',
  },
  imgCardOverlay: {
    position: 'absolute',
    top: '0',
    right: '0',
    bottom: '0',
    left: '0',
    padding: '1.25rem',
  },
}

/* Team Style */

const teamStyle = {
  section: {
    backgroundColor: '#F5F0F0',
    padding: '20px 220px',
    textAlign: 'center',
  },
  title: {
    ...title,
    marginBottom: '1rem',
    marginTop: '30px',
    minHeight: '32px',
    textDecoration: 'none',
  },
  ...imagesStyle,
  itemGrid: {
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  cardTitle,
  smallTitle: {
    color: '#6c757d',
  },
  description: {
    color: '#999',
  },
  justifyCenter: {
    justifyContent: 'center !important',
  },
  socials: {
    marginTop: '0',
    width: '100%',
    transform: 'none',
    left: '0',
    top: '0',
    height: '100%',
    lineHeight: '41px',
    fontSize: '20px',
    color: '#999',
  },
  margin5: {
    margin: '5px',
  },
}

const useStyles = makeStyles(teamStyle)

export default function TeamSection() {
  const classes = useStyles()
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  )
  return (
    <div id="here-is-our-team" className={classes.section}>
      <h2 style={{ fontSize: '2rem' }} className={classes.title}>
        Let's Talk About Us
      </h2>
      <div style={{ textAlign: 'center' }}>
        <GridContainer>
          <GridItem sm={12} md={12} lg={4}>
            <Card plain>
              <GridItem sm={12} md={12} lg={6} className={classes.itemGrid}>
                <img
                  style={{ width: '150px' }}
                  src={Vusal}
                  alt="..."
                  className={imageClasses}
                />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Vusal Ismayilov
                <br />
                <small className={classes.smallTitle}>Lead Developer</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  “having more than 2 years of work and development experience
                  in web, he led us till the last line of code and showed us the
                  right way on how to deal with different hardships making the
                  site”.
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://facebook.com/vusal.ismayilov.1884"
                  target="_blank"
                >
                  <FacebookIcon fontSize="small" />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://github.com/VusalIs"
                  target="_blank"
                >
                  <GitHubIcon fontSize="small" />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://www.linkedin.com/in/vusal-ismayilov-338437154"
                  target="_blank"
                >
                  <LinkedInIcon fontSize="small" />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>

          <GridItem sm={12} md={12} lg={4}>
            <Card plain>
              <GridItem sm={12} md={12} lg={6} className={classes.itemGrid}>
                <img
                  style={{ width: '150px' }}
                  src={Fahmin}
                  alt="..."
                  className={imageClasses}
                />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Fahmin Guliyev
                <br />
                <small className={classes.smallTitle}>
                  Developer & Designer
                </small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  “is a developer who contributed to both back-end and front-end
                  parts of the site.Choosing the best colors and designing the
                  attractive logo he really managed to create an appealing
                  impact on our site”.
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://facebook.com/faxmishok"
                  target="_blank"
                >
                  <FacebookIcon fontSize="small" />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://github.com/faxmishok"
                  target="_blank"
                >
                  <GitHubIcon fontSize="small" />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://linkedin.com/in/fahmin"
                  target="_blank"
                >
                  <LinkedInIcon fontSize="small" />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>

          <GridItem sm={12} md={12} lg={4}>
            <Card plain>
              <GridItem sm={12} md={12} lg={6} className={classes.itemGrid}>
                <img
                  style={{ width: '150px' }}
                  src={Saida}
                  alt="..."
                  className={imageClasses}
                />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Saida Sideif-zada
                <br />
                <small className={classes.smallTitle}>Project Manager</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  “being the multitasking queen who organises the jobs on the
                  project, she kept everything on track and made sure we deliver
                  site's requirements along with setting up the strict policy
                  belonging to the site”.
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://www.facebook.com/profile.php?id=100006905322609"
                >
                  <FacebookIcon fontSize="small" />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://github.com/switchsz"
                  target="_blank"
                >
                  <GitHubIcon fontSize="small" />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://linkedin.com/in/sideifzade"
                  target="_blank"
                >
                  <LinkedInIcon fontSize="small" />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>

          <GridItem style={{ paddingLeft: '245px' }} sm={12} md={12} lg={6}>
            <Card plain>
              <GridItem sm={12} md={12} lg={6} className={classes.itemGrid}>
                <img
                  style={{ width: '150px' }}
                  src={Vusala}
                  alt="..."
                  className={imageClasses}
                />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Vusala Alakbarova
                <br />
                <small className={classes.smallTitle}>Back-end Developer</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  “is a developer who mainly coded the back-end and researched
                  on making the site even more secure.Despite her big roles in
                  the back-end of our site, she has also contributed a lot to
                  the front-end part”.
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <FacebookIcon fontSize="small" />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <GitHubIcon fontSize="small" />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://github.com/vuusale"
                  target="_blank"
                >
                  <LinkedInIcon fontSize="small" />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>

          <GridItem style={{ paddingRight: '245px' }} sm={12} md={12} lg={6}>
            <Card plain>
              <GridItem sm={12} md={12} lg={6} className={classes.itemGrid}>
                <img
                  style={{ width: '150px' }}
                  src={Nihat}
                  alt="..."
                  className={imageClasses}
                />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Nihat Najafov
                <br />
                <small className={classes.smallTitle}>Penetration Tester</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  played a big role in general security analysis of the site.
                  Being site's security responsible person and staying one step
                  ahead of cyber attackers and he took necessary measures to
                  protect and enforce all connections related our site.
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="#"
                  target="_blank"
                >
                  <FacebookIcon fontSize="small" />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="#"
                  target="_blank"
                >
                  <GitHubIcon fontSize="small" />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="#"
                  target="_blank"
                >
                  <LinkedInIcon fontSize="small" />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  )
}
