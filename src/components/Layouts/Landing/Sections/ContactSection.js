import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import GridContainer from '../../../Additional/GridContainer.js'
import GridItem from '../../../Additional/GridItem.js'
import CustomInput from '../../../Additional/CustomInput.js'
import Button from '@material-ui/core/Button'

const title = {
  color: '#F5F0F0',
  margin: '1.75rem 0 0.875rem',
  textDecoration: 'none',
  fontWeight: '700',
  fontFamily: `"Montserrat", sans-serif`,
}

const contactStyle = {
  section: {
    padding: '70px 220px',
  },
  title: {
    ...title,
    marginBottom: '50px',
    marginTop: '30px',
    minHeight: '32px',
    textDecoration: 'none',
    textAlign: 'center',
  },
  description: {
    color: '#999',
    textAlign: 'center',
  },
  textCenter: {
    textAlign: 'center',
  },
  textArea: {
    marginRight: '15px',
    marginLeft: '15px',
  },
}

const useStyles = makeStyles(contactStyle)

export default function ContactSection() {
  const classes = useStyles()
  return (
    <div id="contact-us" className={classes.section}>
      <GridContainer justify="center">
        <GridItem cs={12} sm={12} md={8}>
          <h2 style={{ fontSize: '2rem' }} className={classes.title}>
            Keep in Touch?
          </h2>
          <h4 className={classes.description}>
            Divide details about your product or agency work into parts. Write a
            few lines about each one and contact us about any further
            collaboration. We will respond back to you in a couple of hours.
          </h4>
          <form>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <CustomInput
                  labelText="Your Name"
                  id="name"
                  formControlProps={{
                    fullWidth: true,
                  }}
                />
              </GridItem>
              <GridItem xs={12} sm={12} md={6}>
                <CustomInput
                  labelText="Your Email"
                  id="email"
                  formControlProps={{
                    fullWidth: true,
                  }}
                />
              </GridItem>
              <CustomInput
                labelText="Your Message"
                id="message"
                formControlProps={{
                  fullWidth: true,
                  className: classes.textArea,
                }}
                inputProps={{
                  multiline: true,
                  rows: 5,
                }}
              />
              <GridItem xs={12} sm={12} md={4}>
                <Button
                  variant="contained"
                  style={{ color: '#FFFFFF', backgroundColor: '#c3073f' }}
                >
                  Send Message
                </Button>
              </GridItem>
            </GridContainer>
          </form>
        </GridItem>
      </GridContainer>
    </div>
  )
}
