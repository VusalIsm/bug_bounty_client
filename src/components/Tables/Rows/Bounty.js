// import { Link } from 'react-router-dom';
import React, { Component } from 'react'

import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
// import Input from '@material-ui/core/Input';
// import Search from '@material-ui/icons/Search';

import { withRouter } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import {
  Card,
  CardHeader,
  CardContent,
  Avatar,
  Typography,
} from '@material-ui/core'
import {
  red,
  pink,
  purple,
  deepPurple,
  indigo,
  blue,
  lightBlue,
  cyan,
  teal,
  green,
  lightGreen,
  lime,
  yellow,
  amber,
  orange,
  deepOrange,
  brown,
  grey,
  blueGrey,
} from '@material-ui/core/colors'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepButton from '@material-ui/core/StepButton'

const styles = {
  root: {
    maxHeight: 400,
    minHeight: 370,
    position: 'relative',
  },
  container: {
    // maxHeight: 1000,
  },
  tableRow: {
    '&:hover': {
      backgroundColor: 'lightsteelblue !important',
    },
  },
  completed: {
    display: 'inline-block',
  },
  stepContent: {
    display: 'flex',
    justifyContent: 'center',
  },
  btnPolicy: {
    backgroundColor: '#C3073F',
    color: '#FFF',
    justifySelf: 'stretch',
    borderRadius: '0px',
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
}

function getRandomColor() {
  const colors = [
    red,
    pink,
    purple,
    deepPurple,
    indigo,
    blue,
    lightBlue,
    cyan,
    teal,
    green,
    lightGreen,
    lime,
    yellow,
    amber,
    orange,
    deepOrange,
    brown,
    grey,
    blueGrey,
  ]
  const randomColor = colors[Math.floor(Math.random() * colors.length)]
  return randomColor
}

function getSteps() {
  return ['LOW', 'MEDIUM', 'HIGH']
}

function HorizontalStepper(props) {
  const [activeStep, setActiveStep] = React.useState(0)
  const [completed] = React.useState({})
  const steps = getSteps()

  const handleStep = (step) => () => {
    setActiveStep(step)
  }

  function getStepContent(step) {
    switch (step) {
      case 0:
        return `${props.currency} ${props.low}`
      case 1:
        return `${props.currency} ${props.medium}`
      case 2:
        return `${props.currency} ${props.high}`
      default:
        return 'Unknown'
    }
  }

  return (
    <div style={{ textAlign: 'center' }}>
      <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
        <Stepper nonLinear activeStep={activeStep} style={{ width: '100%' }}>
          {steps.map((label, index) => (
            <Step key={label}>
              <StepButton
                icon=""
                onClick={handleStep(index)}
                completed={completed[index]}
              >
                {label}
              </StepButton>
            </Step>
          ))}
        </Stepper>
      </div>

      <Typography style={{ fontFamily: 'inherit', fontSize: '40px' }}>
        {getStepContent(activeStep)}
      </Typography>
    </div>
  )
}

class Bounty extends Component {
  constructor(props) {
    super(props)
    const { rows } = this.props
    this.state = {
      rows: rows,
    }
  }

  componentWillReceiveProps({ rows }) {
    this.setState({
      rows: rows,
    })
  }

  goToFullPolicy(bountyId) {
    this.props.history.push(`/dashboard/bounty/${bountyId}`)
  }

  render() {
    const { rows } = this.state
    const { classes } = this.props

    return (
      <Grid container spacing={3}>
        {rows.map((row) => {
          var policy = row['policy']
          policy =
            typeof policy === 'string'
              ? `${policy.replace(/^(.{120}[^\s]*).*/, '$1')}...`
              : ''
          var firstDomain = row['domains'][0].domain
          return (
            <Grid item xs={4}>
              <Card className={classes.root}>
                <CardHeader
                  avatar={
                    <Avatar
                      aria-label="avatar"
                      style={{ backgroundColor: getRandomColor()[500] }}
                    >
                      {row['name'].charAt(0)}
                    </Avatar>
                  }
                  title={row['name']}
                  subheader={firstDomain}
                />

                <CardContent>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    {policy}
                  </Typography>
                  <HorizontalStepper
                    currency={row['currency'].short_name}
                    low={row['low_price']}
                    medium={row['medium_price']}
                    high={row['high_price']}
                  />
                </CardContent>

                <Button
                  fullWidth
                  variant="contained"
                  className={classes.btnPolicy}
                  onClick={() => this.goToFullPolicy(row['_id'])}
                >
                  Read Full Policy
                </Button>
              </Card>
            </Grid>
          )
        })}
      </Grid>
    )
  }
}

export default withStyles(styles)(withRouter(Bounty))
