// import { Link } from 'react-router-dom';
import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar'
import { ListSubheader } from '@material-ui/core'
import Link from '@material-ui/core/Link'
import Badge from '@material-ui/core/Badge'

import ROUTES from '../../../shared/helpers/Constants'

const styles = {
  root: {
    // width: '100%',
    // maxWidth: 360,
    // backgroundColor: theme.palette.background.paper,
  },
  container: {
    // maxHeight: 440,
    // fontF: `"Montserrat", sans-serif`,
  },
  bio: {
    color: '#AAAAAA',
    fontSize: '10px',
  },
  customBadge: {
    padding: '0px',
  },
  popover: {
    pointerEvents: 'none',
  },
  paper: {
    padding: '100px',
  },
  linkStyle: {
    color: 'inherit',
  },
}

class User extends Component {
  constructor(props) {
    super(props)
    this.state = {
      rows: [],
    }
  }

  componentWillReceiveProps({ rows }) {
    this.setState({
      rows: rows,
    })
  }

  render() {
    const { rows } = this.state
    const { classes } = this.props
    return (
      <List className={classes.root}>
        <ListItem>
          <ListSubheader>Rank</ListSubheader>

          <ListSubheader style={{ padding: '0px 100px 0px 85px' }}>
            Hacker
          </ListSubheader>

          <ListSubheader style={{ padding: '0px 250px 0px 175px' }}>
            Reputation
          </ListSubheader>
        </ListItem>

        {rows.map((row) => {
          var bio = row['bio']
          bio =
            typeof bio === 'string'
              ? `${bio.replace(/^(.{30}[^\s]*).*/, '$1')}...`
              : ''
          return (
            <Link
              className={classes.linkStyle}
              onClick={(event) =>
                (window.location = `/dashboard/hacker/${row['_id']}`)
              }
            >
              <ListItem button>
                <ListSubheader style={{ marginRight: '25px' }}>
                  #{row['rank']}
                </ListSubheader>

                <ListItemAvatar>
                  {row['status'] === 'PRO' ? (
                    <Badge
                      badgeContent={'PRO'}
                      color="secondary"
                      className={classes.customBadge}
                    >
                      <Avatar
                        alt={row['username'].toUpperCase()}
                        src={`${ROUTES.devURL}/image/avatar/${row['avatar']}`}
                      />
                    </Badge>
                  ) : (
                    <Avatar
                      alt={row['username'].toUpperCase()}
                      src={`${ROUTES.devURL}/image/avatar/${row['avatar']}`}
                    />
                  )}
                </ListItemAvatar>

                <ListItemText
                  style={{ maxWidth: '40%' }}
                  primary={row['username']}
                  secondary={bio}
                />
                <ListItemText>{row['reputation']}</ListItemText>
              </ListItem>
            </Link>
          )
        })}
      </List>
    )
  }
}

export default withStyles(styles)(User)
