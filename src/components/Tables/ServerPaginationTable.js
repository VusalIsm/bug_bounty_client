import React, { Component } from 'react'
import Paper from '@material-ui/core/Paper'
import Pagination from '@material-ui/lab/Pagination'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'

const styles = (theme) => ({
  root: {
    width: '100%',
    height: '100%',
    textAlign: 'center',
  },
  paginate: {
    width: '100%',
    justify: 'center',
    textAlign: 'center',
  },
})

class ServerPaginationTable extends Component {
  constructor(props) {
    super(props)
    const { rowsPerPage } = props
    this.state = {
      page: 1,
      totalNumber: 0,
      rowsPerPage: rowsPerPage,
      rows: [],
    }
    this.loadData = this.loadData.bind(this)
  }

  componentDidMount() {
    this.loadData()
  }

  loadData() {
    const { rowsPerPage, page } = this.state
    const { getData } = this.props
    getData({ limit: rowsPerPage, page }).then((response) => {
      this.setState({
        rows: response.results.results,
        totalNumber: response.results.totalNumber,
      })
    })
  }

  handleChangePage = async (event, newPage) => {
    this.setState({
      page: newPage,
    })
    this.loadData()
  }

  handleChangeRowsPerPage = (event) => {
    this.setState({
      page: 0,
      rowsPerPage: event.target.value,
    })
  }

  render() {
    const { classes } = this.props
    const { page, rowsPerPage, rows, totalNumber } = this.state
    const count = Math.ceil(totalNumber / rowsPerPage)
    return (
      <div>
        {React.cloneElement(this.props.children, { rows: rows })}
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justify="center"
          style={{ padding: '10px' }}
        >
          <Grid item xs={3}>
            <Pagination
              className={classes.paginate}
              count={count}
              page={page}
              onChange={this.handleChangePage}
            />
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default withStyles(styles)(ServerPaginationTable)
