import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import './Buttons.css'

class NavButton extends Component {
  constructor(props) {
    super(props)
    this.goAnchor = this.goAnchor.bind(this)
  }

  goAnchor() {
    const { history, baseUrl, anchor, sendRequest } = this.props
    if (baseUrl) {
      history.push(baseUrl)
      if (anchor)
        setTimeout(
          () =>
            document
              .getElementById(anchor)
              .scrollIntoView({ behavior: 'smooth' }),
          1000
        )
    } else if (sendRequest) {
      sendRequest().then((response) => history.push('/landing'))
    }
  }

  render() {
    const { btnstyle, title } = this.props
    return (
      <button className={btnstyle} onClick={this.goAnchor}>
        {title}
      </button>
    )
  }
}

export default withRouter(NavButton)
