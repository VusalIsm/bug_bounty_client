import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import { green } from '@material-ui/core/colors'
import Button from '@material-ui/core/Button'

const useStyles = makeStyles((theme) => ({
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonSuccess: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
  buttonClass: {
    margin: theme.spacing(3, 0, 2),
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    top: -6,
    left: -6,
    zIndex: 1,
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}))

export default function CircularIntegration(props) {
  const classes = useStyles()
  const { title, sendRequest } = props
  const [loading, setLoading] = React.useState(false)

  const handleButtonClick = () => {
    if (!loading) {
      setLoading(true)
      sendRequest().finally(() => {
        setLoading(false)
      })
    }
  }

  return (
    <div className={classes.wrapper}>
      <Button
        fullWidth
        variant="contained"
        color="primary"
        className={classes.buttonClass}
        disabled={loading}
        onClick={handleButtonClick}
      >
        {title}
      </Button>
      {loading && (
        <CircularProgress size={24} className={classes.buttonProgress} />
      )}
    </div>
  )
}
