import { signoutAccount } from '../src/shared/helpers/Request'

var routes = {
  HACKER_DASHBOARD: [
    {
      id: 1,
      name: 'Hacker Leaderboard',
      path: '/dashboard/leaderboard',
    },
    {
      id: 2,
      name: 'List of Bounties',
      path: '/dashboard/bounties',
    },
    {
      id: 3,
      name: 'My submissions',
      path: '/dashboard/submissions',
    },
    {
      id: 4,
      name: 'My profile',
      path: '/dashboard/hacker-profile',
    },
    {
      id: 5,
      name: 'Sign out',
      request: signoutAccount,
    },
  ],
  COMPANY_DASHBOARD: [
    {
      id: 1,
      name: 'Join Bug Bounty',
      path: '/dashboard/join-bug-bounty',
    },
    {
      id: 2,
      name: 'Hacker Leaderboard',
      path: '/dashboard/leaderboard',
    },
    {
      id: 3,
      name: 'Submissions',
      path: '/dashboard/bounty-submissions',
    },
    {
      id: 4,
      name: 'My profile',
      path: '/dashboard/company-profile',
    },
    {
      id: 5,
      name: 'Sign out',
      request: signoutAccount,
    },
  ],
}

export default routes
