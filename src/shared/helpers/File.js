function downloadMessageAsPlainText(text, fileName) {
  const element = document.createElement('a')
  const file = new Blob([text], { type: 'text/plain' })
  element.href = URL.createObjectURL(file)
  element.download = fileName
  document.body.appendChild(element)
  element.click()
}

export { downloadMessageAsPlainText }
