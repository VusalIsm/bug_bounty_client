import axios from 'axios'
import ROUTES from './Constants'

const instance = axios.create({
  baseURL: ROUTES.backendURL,
  // baseURL: ROUTES.devURL,
  timeout: 1000,
  withCredentials: true,
})

instance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error.response.status === 401) {
      window.location = '/landing/login'
    }
    return Promise.reject(error.response.data)
  }
)

function loginHacker(data) {
  return instance.post('/auth/hacker/login', data)
}

function loginCompany(data) {
  return instance.post('/auth/company/login', data)
}

function signoutAccount() {
  return instance.post('/auth/signout')
}

function registerHacker(data) {
  return instance.post('/auth/hacker/register', data)
}

function registerCompany(data) {
  return instance.post('/auth/company/register', data)
}

function getHackerLeaderboard(requestParams) {
  const params = { params: requestParams }
  return instance.get('/hacker/leaderboard', params)
}

function getHackerAvatar(requestParams) {
  const avatar = requestParams.avatar
  return instance.get(`/image/avatar/${avatar}`)
}

function getBounties(requestParams) {
  const params = { params: requestParams }
  return instance.get('/hacker/bounties', params)
}

function getBountyById(bountyId) {
  return instance.get(`/hacker/bounty/${bountyId}`)
}

function getBountyForHackers(requestParams) {
  const id = requestParams.id
  return instance.get(`/hacker/bounty/${id}`)
}

function getHackerProfile(requestParams) {
  const id = requestParams.id
  return instance.get(`/profile/hacker/${id}`)
}

function getCompanyProfile(requestParams) {
  const id = requestParams.id
  return instance.get(`/profile/company/${id}`)
}

function getHackerDB(data) {
  return instance.get(`/profile/hacker/dashboard`, data)
}

function updateHackerDB(data) {
  return instance.put(`/profile/hacker/dashboard`, data)
}

function getDomainsByUser() {
  return instance.get('/profile/company/domains')
}

function getAllCurrencies() {
  return instance.get('/profile/currencies')
}

function createBounty(data) {
  return instance.post('/profile/company/bounty/create', data)
}

function getResponsibleInfo() {
  return instance.get('/profile/responsible/dashboard')
}

function getCompanyInfo() {
  return instance.get('/profile/company/dashboard')
}

function verifyDomain(id) {
  return instance.post(`/domain/verify/${id}`)
}

function createDomain(data) {
  return instance.post(`/domain/create`, data)
}

function savePublicKey(data) {
  return instance.post(`/profile/key`, data)
}

function getPublicKey(companyId) {
  return instance.get(`/profile/company/key/${companyId}`)
}

function createNewReport(data) {
  return instance.post(`/hacker/report/create`, data)
}

function getAllSubmittedReportsForHacker() {
  return instance.get(`/hacker/reports`)
}

function getAllSubmittedReportsForCompany() {
  return instance.get(`/profile/reports`)
}

export {
  loginHacker,
  loginCompany,
  registerHacker,
  registerCompany,
  signoutAccount,
  getHackerLeaderboard,
  getHackerAvatar,
  getBounties,
  getBountyForHackers,
  getBountyById,
  getHackerProfile,
  getCompanyProfile,
  getHackerDB,
  updateHackerDB,
  getDomainsByUser,
  getAllCurrencies,
  createBounty,
  getResponsibleInfo,
  getCompanyInfo,
  verifyDomain,
  createDomain,
  savePublicKey,
  getPublicKey,
  createNewReport,
  getAllSubmittedReportsForHacker,
  getAllSubmittedReportsForCompany,
}
