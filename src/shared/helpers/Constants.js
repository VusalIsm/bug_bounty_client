const ROUTES = Object.freeze({
  backendURL: 'https://bug-bounty-client.herokuapp.com',
  devURL: 'http://localhost:5000',
})

export default ROUTES
