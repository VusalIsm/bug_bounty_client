import {
  convertBinaryToPem,
  convertPemToBinary,
  textToArrayBuffer,
  arrayBufferToBase64String,
  base64StringToArrayBuffer,
  arrayBufferToText,
} from './Converter'

function generateKeyPair() {
  return crypto.subtle.generateKey(
    {
      name: 'RSA-OAEP',
      modulusLength: 2048,
      publicExponent: new Uint8Array([1, 0, 1]),
      extractable: false,
      hash: {
        name: 'SHA-256',
      },
    },
    true,
    ['encrypt', 'decrypt']
  )
}

async function generateAndExportKeys() {
  const keys = await generateKeyPair()
  const keyStrings = await Promise.all([
    exportPublicKey(keys),
    exportPrivateKey(keys),
  ])
  return {
    publicKey: keyStrings[0],
    privateKey: keyStrings[1],
  }
}

function importPublicKey(pemKey) {
  return crypto.subtle.importKey(
    'spki',
    convertPemToBinary(pemKey),
    {
      name: 'RSA-OAEP',
      modulusLength: 2048,
      publicExponent: new Uint8Array([1, 0, 1]),
      extractable: true,
      hash: {
        name: 'SHA-256',
      },
    },
    true,
    ['encrypt']
  )
}

function importPrivateKey(pemKey) {
  return crypto.subtle.importKey(
    'pkcs8',
    convertPemToBinary(pemKey),
    {
      name: 'RSA-OAEP',
      modulusLength: 2048,
      publicExponent: new Uint8Array([1, 0, 1]),
      extractable: false,
      hash: {
        name: 'SHA-256',
      },
    },
    true,
    ['decrypt']
  )
}

function exportPublicKey(keys) {
  return window.crypto.subtle
    .exportKey('spki', keys.publicKey)
    .then((spki) => convertBinaryToPem(spki, 'RSA PUBLIC KEY'))
}

function exportPrivateKey(keys) {
  return window.crypto.subtle
    .exportKey('pkcs8', keys.privateKey)
    .then((pkcs8) => convertBinaryToPem(pkcs8, 'RSA PRIVATE KEY'))
}

function exportPemKeys(keys) {
  return new Promise(function (resolve) {
    exportPublicKey(keys).then(function (pubKey) {
      exportPrivateKey(keys).then(function (privKey) {
        resolve({ publicKey: pubKey, privateKey: privKey })
      })
    })
  })
}

async function encryptData(key, data) {
  const vector = crypto.getRandomValues(new Uint8Array(16))
  const public_key = await importPublicKey(key)

  return crypto.subtle
    .encrypt(
      {
        name: 'RSA-OAEP',
        iv: vector,
      },
      public_key,
      textToArrayBuffer(data)
    )
    .then((resultBuffer) => arrayBufferToBase64String(resultBuffer))
}

async function decryptData(key, data) {
  const vector = crypto.getRandomValues(new Uint8Array(16))
  const private_key = await importPrivateKey(key)
  return crypto.subtle
    .decrypt(
      {
        name: 'RSA-OAEP',
        iv: vector,
      },
      private_key,
      base64StringToArrayBuffer(data)
    )
    .then((resultBuffer) => arrayBufferToText(resultBuffer))
}

export {
  generateKeyPair,
  generateAndExportKeys,
  importPublicKey,
  importPrivateKey,
  exportPublicKey,
  exportPrivateKey,
  exportPemKeys,
  encryptData,
  decryptData,
}
